package finalPage;

import com.example.services.UserApiService;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;

import static com.example.conditions.Conditions.bodyField;
import static com.example.conditions.Conditions.statusCode;
import static io.restassured.RestAssured.get;
import static org.hamcrest.Matchers.*;

public class FinalPageApi {
    private final UserApiService userApiService = new UserApiService();


    @Test
    public void testFinalPageApi(){
        get("https://auto.ria.com/demo/bu/searchPage/v2/view/auto/23904878?lang_id=2")
                .then().statusCode(200)
                .and()
                .assertThat()
//                .body("userIbhbjhjd", equalTo(true))
                .body("userId", notNullValue(String.class));

        userApiService.sendGet("https://auto.ria.com/demo/bu/searchPage/v2/view/auto/23904878?lang_id=2")
                .shouldHave(statusCode(200))

                .shouldHave(bodyField("userId", is(not(empty()))))
                .shouldHave(bodyField("userBlocked", is((empty()))))
                .shouldHave(bodyField("chipsCount", is(not(empty()))))
                .shouldHave(bodyField("locationCityName", is(not(empty()))))
                .shouldHave(bodyField("cityLocative", is(not(empty()))))
                .shouldHave(bodyField("auctionPossible", is(not(empty()))))
                .shouldHave(bodyField("exchangePossible", is(not(empty()))))
                .shouldHave(bodyField("realtyExchange", is(not(empty()))))
                .shouldHave(bodyField("exchangeType", is(not(empty()))))
                .shouldHave(bodyField("exchangeTypeId", is(not(empty()))))
                .shouldHave(bodyField("updateDate", is(not(empty()))))
                .shouldHave(bodyField("expireDate", is(not(empty()))))
                .shouldHave(bodyField("soldDate", is(not(empty()))))
                .shouldHave(bodyField("userHideADSStatus", is(not(empty()))))
                .shouldHave(bodyField("userPhoneData.phoneId", is(not(empty()))))
                .shouldHave(bodyField("userPhoneData.phone", is(not(empty()))))
                .shouldHave(bodyField("USD", is(not(empty()))))
                .shouldHave(bodyField("UAH", is(not(empty()))))
                .shouldHave(bodyField("EUR", is(not(empty()))))
                .shouldHave(bodyField("isAutoAddedByPartner", is(not(empty()))))
                .shouldHave(bodyField("partnerId", is(not(empty()))))
                .shouldHave(bodyField("levelData.level", is(not(empty()))))
                .shouldHave(bodyField("levelData.label", is(not(empty()))))
                .shouldHave(bodyField("levelData.hotType", is(not(empty()))))
                .shouldHave(bodyField("levelData.expireDate", is(not(empty()))))

                .shouldHave(bodyField("autoData.active", is(not(empty()))))
                .shouldHave(bodyField("autoData.description", is(not(empty()))))
                .shouldHave(bodyField("autoData.version", is(not(empty()))))
                .shouldHave(bodyField("autoData.onModeration", is(not(empty()))))
                .shouldHave(bodyField("autoData.year", is(not(empty()))))
                .shouldHave(bodyField("autoData.bodyId", is(not(empty()))))
                .shouldHave(bodyField("autoData.statusId", is(not(empty()))))
                .shouldHave(bodyField("autoData.withVideo", is(not(empty()))))
                .shouldHave(bodyField("autoData.race", is(not(empty()))))
                .shouldHave(bodyField("autoData.raceInt", is(not(empty()))))
                .shouldHave(bodyField("autoData.fuelName", is(not(empty()))))
                .shouldHave(bodyField("autoData.fuelNameEng", is(not(empty()))))
                .shouldHave(bodyField("autoData.gearboxName", is(not(empty()))))
                .shouldHave(bodyField("autoData.isSold", is(not(empty()))))
                .shouldHave(bodyField("autoData.mainCurrency", is(not(empty()))))
                .shouldHave(bodyField("autoData.fromArchive", is(not(empty()))))
                .shouldHave(bodyField("autoData.categoryId", is(not(empty()))))
                .shouldHave(bodyField("autoData.categoryNameEng", is(not(empty()))))
                .shouldHave(bodyField("autoData.subCategoryNameEng", is(not(empty()))))
                .shouldHave(bodyField("autoData.custom", is(not(empty()))))

                .shouldHave(bodyField("markName", is(not(empty()))))
                .shouldHave(bodyField("markNameEng", is(not(empty()))))
                .shouldHave(bodyField("markId", is(not(empty()))))
                .shouldHave(bodyField("modelName", is(not(empty()))))
                .shouldHave(bodyField("modelNameEn", is(not(empty()))))
                .shouldHave(bodyField("modelId", is(not(empty()))))
                .shouldHave(bodyField("photoData.count", is(not(empty()))))
                .shouldHave(bodyField("photoData.seoLinkM", is(not(empty()))))
                .shouldHave(bodyField("photoData.seoLinkSX", is(not(empty()))))
                .shouldHave(bodyField("photoData.seoLinkB", is(not(empty()))))
                .shouldHave(bodyField("photoData.seoLinkF", is(not(empty()))))
                .shouldHave(bodyField("linkToView", is(not(empty()))))
                .shouldHave(bodyField("title", is(not(empty()))))
                .shouldHave(bodyField("stateData.name", is(not(empty()))))
                .shouldHave(bodyField("stateData.regionName", is(not(empty()))))
                .shouldHave(bodyField("stateData.regionNameEng", is(not(empty()))))
                .shouldHave(bodyField("stateData.linkToCatalog", is(not(empty()))))
                .shouldHave(bodyField("stateData.title", is(not(empty()))))
                .shouldHave(bodyField("stateData.stateId", is(not(empty()))))
                .shouldHave(bodyField("stateData.cityId", is(not(empty()))))

                .shouldHave(bodyField("oldTop.isActive", is(not(empty()))))
                .shouldHave(bodyField("oldTop.expireDate", is(not(empty()))))

                .shouldHave(bodyField("canSetSpecificPhoneToAdvert", is(not(empty()))))
                .shouldHave(bodyField("dontComment", is(not(empty()))))
                .shouldHave(bodyField("sendComments", is(not(empty()))))
//                .shouldHave(bodyField("badges", is("null"))) ////подивитись, як зразок

                .shouldHave(bodyField("checkedVin.vin", is(not(empty()))))
                .shouldHave(bodyField("checkedVin.isShow", is(not(empty()))))
                .shouldHave(bodyField("checkedVin.hasRestrictions", is(not(empty()))))
                .shouldHave(bodyField("checkedVin.checkDate", is(not(empty()))))
                .shouldHave(bodyField("checkedVin.checkDate", is(not(empty()))))

                .shouldHave(bodyField("hasWebP", is(not(empty()))))
                .shouldHave(bodyField("moderatedAbroad", is(not(empty()))))
                .shouldHave(bodyField("secureKey", is(not(empty()))))
                .shouldHave(bodyField("isLeasing", is(not(empty()))))

                .shouldHave(bodyField("dealer.link", is(not(empty()))))
                .shouldHave(bodyField("dealer.logo", is(not(empty()))))
                .shouldHave(bodyField("dealer.type", is(not(empty()))))
                .shouldHave(bodyField("dealer.id", is(not(empty()))))
                .shouldHave(bodyField("dealer.name", is(not(empty()))))
                .shouldHave(bodyField("dealer.packageId", is(not(empty()))))
                .shouldHave(bodyField("dealer.typeId", is(not(empty()))))

                .shouldHave(bodyField("withInfoBar", is(not(empty()))))
                .shouldHave(bodyField("infoBarText", is(not(empty()))))
                .shouldHave(bodyField("optionStyles", is((empty()))))

//                .shouldHave(bodyField("userId", is((empty()))))

        ;


    }

}
