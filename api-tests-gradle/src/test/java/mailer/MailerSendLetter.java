package mailer;

import com.example.services.UserApiService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.stream.Stream;

import static com.example.conditions.Conditions.statusCode;


public class MailerSendLetter {
    private final UserApiService userApiService = new UserApiService();


    @Test
    public void testMailer() {
        String file = System.getProperty("user.dir") + "/src/test/java/mailer/mailetTempId.txt";

        try {
            Stream<String> stream = Files.lines(Paths.get(file));
            stream.forEach(k -> {
                System.out.println(k);


                String string = userApiService.sendGet("http://mailer.ria.com/rest/send/tid/" + k + "/")

                        .jsonPath()
                        .and()
                        .getString("data.date").substring(1, 11);

                LocalDate date = LocalDate.parse(string);
                System.out.println(date);
                System.out.println(java.time.LocalDate.now());

                if (!date.equals(java.time.LocalDate.now())) {
                    try {
                        write_to_file("/src/test/java/mailer/mailetTempIdRes.txt", k);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void write_to_file(String dir, String text) throws Exception {
        try (FileWriter writer = new FileWriter(System.getProperty("user.dir") + dir, true)) {
// запись всей строки
            writer.write(text + "\n");

            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

}
