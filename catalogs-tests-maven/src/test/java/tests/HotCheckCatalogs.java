package tests;

import org.testng.annotations.Test;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

public class HotCheckCatalogs {
    String catalogs[] = {
            "https://auto.ria.com/last/hour/",
            "https://auto.ria.com/last/today/",
            "https://auto.ria.com/rastamozhka-avto/",
            "https://auto.ria.com/driven-cars/",
            "https://auto.ria.com/driven-cars/austria/",
            "https://auto.ria.com/driven-cars/vaz/",
            "https://auto.ria.com/avtoobmen/",
            "https://auto.ria.com/after-dtp/",
            "https://auto.ria.com/after-dtp/daewoo/",
            "https://auto.ria.com/after-dtp/honda/accord/",
            "https://auto.ria.com/car/",
            "https://auto.ria.com/moto-catalog/",
            "https://auto.ria.com/year-catalog/",
            "https://auto.ria.com/legkovie/",
            "https://auto.ria.com/legkovie/sedan/",
            "https://auto.ria.com/legkovie/vaz/",
            "https://auto.ria.com/spectehnika/city/kiev/",
            "https://auto.ria.com/legkovie/state/uzhgorod/",
            "https://auto.ria.com/car/renault/",
            "https://auto.ria.com/car/mercedes-benz/e-class/",
            "https://auto.ria.com/car/mercedes-benz/city/vinnica/",
            "https://auto.ria.com/car/mazda/state/poltava/",
            "https://auto.ria.com/state/ivano-frankovsk/",
            "https://auto.ria.com/city/vinnica/",
            "https://auto.ria.com/car/year/2013/",
            "https://auto.ria.com/car/city/kherson/year/2013/",
            "https://auto.ria.com/car/mercedes-benz/year/2012/",
            "https://auto.ria.com/car/audi/a6/year/2012/",
            "https://auto.ria.com/car/fuel/benzin/",
            "https://auto.ria.com/car/mercedes-benz/fuel/benzin/",
            "https://auto.ria.com/car/audi/fuel/benzin/engine/3000/",
            "https://auto.ria.com/car/mercedes-benz/e-class/fuel/benzin/",
            "https://auto.ria.com/car/mercedes-benz/e-class/fuel/benzin/engine/3000/",
            "https://auto.ria.com/car/bmw/engine/3000/",
            "https://auto.ria.com/car/bmw/330/engine/3000/",
            "https://auto.ria.com/kpp/avtomat/",
            "https://auto.ria.com/car/mercedes-benz/kpp/avtomat/",
            "https://auto.ria.com/car/audi/a6/kpp/avtomat/",
            "https://auto.ria.com/cost/cheap/",
            "https://auto.ria.com/car/chevrolet/cost/cheap/",
            "https://auto.ria.com/car/chevrolet/lacetti/cost/cheap/",
            "https://auto.ria.com/price/25000/",
            "https://auto.ria.com/country/germany/",
            "https://auto.ria.com/color/asphalt/",
            "https://auto.ria.com/car/audi/color/asphalt/",
            "https://auto.ria.com/car/audi/a6/color/asphalt/",
            "https://auto.ria.com/avto-na-zapchasti/",
            "https://auto.ria.com/avto-na-zapchasti/alfa-romeo/",
            "https://auto.ria.com/car/audi/photo/",
            "https://auto.ria.com/car/audi/a6/photo/",
            "https://auto.ria.com/legkovie/photo/",
            "https://auto.ria.com/legkovie/sedan/photo/",
            "https://auto.ria.com/legkovie/bmw/photo/",
            "https://auto.ria.com/legkovie/sedan/vaz/photo/",
            "https://auto.ria.com/car/bmw/price/",
            "https://auto.ria.com/car/bmw/x5/price/",
            "https://auto.ria.com/bus/price/",
            "https://auto.ria.com/moto/mopedy/price/",
            "https://auto.ria.com/moto/bmw/price/",
            "https://auto.ria.com/city/kiev/price/",
            "https://auto.ria.com/car/bmw/city/kiev/price/",
            "https://auto.ria.com/car/tth/",
            "https://auto.ria.com/car/volkswagen/tth/",
            "https://auto.ria.com/car/volkswagen/passat-b5/tth/",
            "https://auto.ria.com/car/volkswagen/passat-b5/130552/tth/"};

    String catalogSearch[] = {"https://auto.ria.com/rastamozhka-avto/?page=1",
            "https://auto.ria.com/driven-cars/?page=1",
            "https://auto.ria.com/driven-cars/austria/?page=1",
            "https://auto.ria.com/driven-cars/vaz/?page=1",
            "https://auto.ria.com/avtoobmen/?page=1",
            "https://auto.ria.com/after-dtp/?page=1",
            "https://auto.ria.com/after-dtp/daewoo/?page=1",
            "https://auto.ria.com/after-dtp/honda/accord/?page=1",
            "https://auto.ria.com/moto-catalog/?page=1",
            "https://auto.ria.com/year-catalog/?page=1",
            "https://auto.ria.com/legkovie/?page=1",
            "https://auto.ria.com/legkovie/sedan/?page=1",
            "https://auto.ria.com/legkovie/vaz/?page=1",
            "https://auto.ria.com/spectehnika/city/kiev/?page=1",
            "https://auto.ria.com/legkovie/state/uzhgorod/?page=1",
            "https://auto.ria.com/car/mercedes-benz/?page=1",
            "https://auto.ria.com/car/mercedes-benz/e-class/?page=1",
            "https://auto.ria.com/car/mercedes-benz/city/vinnica/?page=1",
            "https://auto.ria.com/car/mazda/state/poltava/?page=1",
            "https://auto.ria.com/state/ivano-frankovsk/?page=1",
            "https://auto.ria.com/city/vinnica/?page=1",
            "https://auto.ria.com/car/year/2013/?page=1",
            "https://auto.ria.com/car/city/kherson/year/2013/?page=1",
            "https://auto.ria.com/car/mercedes-benz/year/2012/?page=1",
            "https://auto.ria.com/car/audi/a6/year/2012/?page=1",
            "https://auto.ria.com/car/fuel/benzin/?page=1",
            "https://auto.ria.com/car/mercedes-benz/fuel/benzin/?page=1",
            "https://auto.ria.com/car/audi/fuel/benzin/engine/3000/?page=1",
            "https://auto.ria.com/car/mercedes-benz/e-class/fuel/benzin/?page=1",
            "https://auto.ria.com/car/mercedes-benz/e-class/fuel/benzin/engine/3000/?page=1",
            "https://auto.ria.com/car/bmw/engine/3000/?page=1",
            "https://auto.ria.com/car/bmw/330/engine/3000/?page=1",
            "https://auto.ria.com/kpp/avtomat/?page=1",
            "https://auto.ria.com/car/mercedes-benz/kpp/avtomat/?page=1",
            "https://auto.ria.com/car/audi/a6/kpp/avtomat/?page=1",
            "https://auto.ria.com/cost/cheap/?page=1",
            "https://auto.ria.com/car/chevrolet/cost/cheap/?page=1",
            "https://auto.ria.com/car/chevrolet/lacetti/cost/cheap/?page=1",
            "https://auto.ria.com/country/germany/?page=1",
            "https://auto.ria.com/color/asphalt/?page=1",
            "https://auto.ria.com/car/audi/color/asphalt/?page=1",
            "https://auto.ria.com/car/audi/a6/color/asphalt/?page=1",
            "https://auto.ria.com/avto-na-zapchasti/?page=1",
            "https://auto.ria.com/avto-na-zapchasti/alfa-romeo/?page=1"};

    @Test(priority = 1)
    public void testHotCheckCatalogs() {

        Arrays.stream(catalogs).forEach(k -> {

            try {
                int stutusCode = sendGet(k);
                if (stutusCode != 200) {
                    System.out.println(stutusCode + "  --->  " + k);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        });
    }

    @Test(priority = 2)
    public void testHotCheckCatalogSearch() {

        Arrays.stream(catalogSearch).forEach(k -> {

            try {
                int stutusCode = sendGet(k);
                if (stutusCode != 200) {
                    System.out.println(stutusCode + "  --->  " + k);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    private int sendGet(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        return responseCode;
    }
}
