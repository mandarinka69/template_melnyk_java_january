package com.auto.ui.tests;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

public class FinalPageConvertMD5 {


    @Test
    public void testCheckFirstAds() throws IOException{

        List<Object> arrayId = RestAssured.when().get("https://auto.ria.com/blocks_search_ajax/search/?noCache=1&page=1")
//                .jsonPath().getInt("result.search_result.ids").size();
                .jsonPath()
                .and()
                .getList("result.search_result.ids");

        arrayId.stream().forEach(k -> {

            String apiFinalPage = getHashedUrlForNewAutoData(Integer.parseInt(k.toString()), "userDataSecureSalt", 2);
            System.out.println(apiFinalPage);

            Response response = null;
            try {
                response = Jsoup.connect(apiFinalPage).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(response.statusCode() + " : " + response.url());


        });





        System.out.println(

                getHashedUrlForNewAutoData(23782089, "userDataSecureSalt", 2)
        );

    }

    @Test
    public void test2CheckLastAds(){
        int countAds = RestAssured.when().get("https://auto.ria.com/blocks_search_ajax/search/?noCache=1&page=1")
                .jsonPath()
                .getInt("result.search_result.count");
        int pageToGetAds = countAds/10;

        List<Object> arrayId = RestAssured.when().get("https://auto.ria.com/blocks_search_ajax/search/?noCache=1&page="+pageToGetAds+"")
//                .jsonPath().getInt("result.search_result.ids").size();
                .jsonPath()
                .and()
                .getList("result.search_result.ids");

        arrayId.stream().forEach(k -> {

            String apiFinalPage = getHashedUrlForNewAutoData(Integer.parseInt(k.toString()), "userDataSecureSalt", 2);
            System.out.println(apiFinalPage);

            ValidatableResponse statusCode = RestAssured.when().get(apiFinalPage).then().statusCode(200);
            System.out.println(statusCode.toString());
        });

        System.out.println(countAds);
    }

    public static String getHashedUrlForNewAutoData(int advertId, String userDataSalt, int langCode) {
        String hash_str = "31536000" + advertId + " " + userDataSalt;
        String hash_md5 = generateMD5Base64(hash_str);
        return "http://api.mobile.rest.auto.ria.com/advertisement/" + advertId + "?hash=" + hash_md5 + "&expires=31536000&langId=" + langCode;
    }


    public static String generateMD5Base64(String input) {
        String md5Sum = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());
            byte[] enc = md.digest();
            md5Sum = StringUtils.trim(Base64.getUrlEncoder().withoutPadding().encodeToString(enc));
        } catch (NoSuchAlgorithmException nsae) {
            System.out.println(nsae.getMessage());
        }
        return md5Sum;
    }

}
