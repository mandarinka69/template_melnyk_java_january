package com.auto.ui.tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.example.services.UserApiService;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.socks.ui.tests.BaseUiTest.*;


public class FindNotValidResponsePhotoFirstStep {
    List<String> result = new ArrayList<String>();
    String jsonFinalPage;

    @BeforeClass
    public void setUp() {

        Configuration.browser = "chrome";
        Configuration.browserSize = "1366x768";
    }


    @DataProvider(name = "photo_id", parallel = true)
    public static Iterator<Object[]> urls() throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/new_ads_id.csv"));
//        BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/test/java/com/socks/ui/tests/auto_id.csv"));
        String line;
        List<Object[]> lines = new ArrayList<>();

        while ((line = in.readLine()) != null) {
            lines.add(new String[]{line});
        }

        return lines.iterator();

    }

/*    @Test(dataProvider = "photo_id")
    public void photoMustReturn200(String id) throws Exception {

        Selenide.open("https://auto.ria.com/auto___" + id + ".html");
        System.out.println(id);

        Selenide.$$("#photosBlock > div.gallery-order.carousel > div.carousel-inner._flex > div > picture > source").stream()
                .forEach(k -> {

                    //System.out.println(k.attr("srcset"));

                    int statusCode = new HttpResponseCode().httpResponseCodeViaGet(k.attr("srcset"));

                    if (statusCode != 200) {

                        System.out.println("    " + k.attr("srcset") + " " + statusCode);
                        System.out.println("----------");
                        String photoID = returnPhotoID(k.attr("srcset"));
                        System.out.println(photoID);

                        try {
                            write_to_file("/src/test/java/com/socks/ui/tests/new_ads_id_result.txt", id);
                            write_to_file("/src/test/java/com/socks/ui/tests/photo_id_result.txt", photoID);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
*/


    private final UserApiService userApiService = new UserApiService();

   @Test(dataProvider = "photo_id")
   public void testSecondVersion(String id) throws Exception{
   //@Test()
    //public void testSecondVersion() throws Exception {


        int autoId;
        int photoId;
        String small;
        String mid;
        String large;
        String huge;
        String largeWebp;
        String hugeWebp;

        JSONObject json = read_json_from_url("https://auto.ria.com/demo/bu/finalPage/photos/" + id + "");
        //JSONObject json = read_json_from_url("https://auto.ria.com/demo/bu/finalPage/photos/24218801");
        int count_photo = json.getJSONArray("photo").length();

        for (int i = 0; i < count_photo; i++) {
            Object jsonArray = json.getJSONArray("photo").getJSONObject(i);





            photoId = ((JSONObject) jsonArray).getInt("id");
            small = ((JSONObject) jsonArray).getString("small");



            int statusCode = new HttpResponseCode().httpResponseCodeViaGet(small);
            /**записуємо ід оголошення**/
            if (statusCode != 200 && statusCode != 415){
                String linkForDelete = "http://photos.rest.auto.ria.ua/photos/" + photoId + "";
                int autoIdFromJson = read_json_from_url(linkForDelete).getJSONObject("data").getJSONObject(Integer.toString(photoId)).getInt("auto_id");
                System.out.println(autoIdFromJson);
                write_to_file("/src/test/java/com/auto/ui/tests/id_ads_photo_404.txt", Integer.toString(autoIdFromJson));
            }


            /**видалення фото**/

            if (statusCode != 200 && statusCode != 415) {
                String linkForDelete = "http://photos.rest.auto.ria.ua/photos/" + photoId + "";

                int autoIdFromJson = read_json_from_url(linkForDelete).getJSONObject("data").getJSONObject(Integer.toString(photoId)).getInt("auto_id");
                //autoId = linkForDelete..getInt("auto_id");
                System.out.println(autoIdFromJson);
                //System.out.println(id);
                System.out.println(photoId + " " + statusCode);


                userApiService.sendDelete(linkForDelete);
                /*System.out.println("------------------");
                System.out.println(getJsonFinalPage(Integer.toString(autoIdFromJson)));

                int autoIdFromJsonFinalPAge = read_json_from_url(getJsonFinalPage(id)).getInt("userId");

                System.out.println(autoIdFromJsonFinalPAge + "<-----");

                String bodyMessade = "userId: "+ autoIdFromJsonFinalPAge+ "autoId: " + autoIdFromJson + " photoId:" + photoId + " " + statusCode;
                write_to_file("/src/test/java/com/auto/ui/tests/photo_id_result.txt", bodyMessade);
                write_to_file("/src/test/java/com/auto/ui/tests/auto_id_result.txt", Integer.toString(autoIdFromJson));
                write_to_file("/src/test/java/com/auto/ui/tests/user_id_result.txt", Integer.toString(autoIdFromJsonFinalPAge));
                */

                Thread.sleep(100);
            }
        }
    }


    @Test(priority = 2)
    public void testDeleteDupesAutoID() throws Exception {
        List<String> withoutDupes = Arrays.stream(read_from_url_to_array(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/id_ads_photo_404_doupless.txt"))
                .distinct()
                .collect(Collectors.toList());

        withoutDupes.stream().forEach(k -> {
            System.out.println(k);
            try {
                write_to_file("/src/test/java/com/auto/ui/tests/photo_id_result_without_dupes.txt", k);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Test(priority = 3)
    public void testDeleteDupesUserID() throws Exception {
        List<String> withoutDupes = Arrays.stream(read_from_url_to_array(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/user_id_result.txt"))
                .distinct()
                .collect(Collectors.toList());

        withoutDupes.stream().forEach(k -> {
            System.out.println(k);
            try {
                write_to_file("/src/test/java/com/auto/ui/tests/user_id_result_without_dupes.txt", k);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }



    @Test
    public void testDelete() throws Exception {

        Arrays.stream(read_from_url_to_array(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/photo_id_result_without_dupes.txt"))
                .forEach(k -> {
                    System.out.println(k);
                    String linkForDelete = "http://photos.rest.auto.ria.ua/photos/" + k + "";
                    userApiService.sendDelete(linkForDelete);
                });



    }


    public String getJsonFinalPage(String id) throws Exception {
        final int subLenFirst = 6;
        final int subLenSecond = 4;
        final int scaleFirst = 100;
        final int divSecond = 10000;

        String idFirst = id;
        int idSecondInt = Integer.parseInt(id.substring(0, subLenFirst))
                + roundString(id.substring(subLenFirst), scaleFirst);
        String idSecond = Integer.toString(idSecondInt);

        int ThreeInt = Integer.parseInt(id.substring(0, subLenSecond))
                + roundString(id.substring(subLenSecond), divSecond);
        String idThree = Integer.toString(ThreeInt);

        return jsonFinalPage = "https://c-ua1.riastatic.com/demo/bu/searchPage/v2/view/auto/" + idThree + "/" + idSecond + "/" + id + "?lang_id=2";

    }


    public int roundString(String string, int scale) {
        return Math.round(Float.parseFloat(string) / scale);
    }

    public String returnPhotoID(String link) {
        String line = link;

        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(line);

        while (m.find()) {

            result.add(m.group());
        }

        int count = result.size() - 1;
        return result.get(count);
    }


}
