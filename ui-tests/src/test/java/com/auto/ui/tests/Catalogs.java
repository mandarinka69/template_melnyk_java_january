package com.auto.ui.tests;

import com.example.assertions.AssertableResponse;
import com.example.services.UserApiService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.codeborne.selenide.Selenide.open;

public class Catalogs {
    private UserApiService userApiService = new UserApiService();
    CatalogPage catalogPage = new CatalogPage();

    private int invalidLinksCount;
    @BeforeAll
    public static void setUp() {

        //адекватно зробити!!!!!!


/*        Map myVars = new HashMap();
        myVars.put("env", System.getProperty("env", "prod"));

        RestAssured.baseURI = ConfigFactory.create(ProjectConfig.class, myVars).apiPath();

        Configuration.browser = "chrome";
        Configuration.browserSize = "1366x768";
*/

    }

    @Test
    public void testCheckResponceFromCatalogs() {
//        open("https://auto.ria.com/car/renault/");


        String file = System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/catalogs_param.csv";

        List<String[]> rowList = new ArrayList<String[]>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineItems = line.split(",");
                rowList.add(lineItems);
            }

        }

        catch(Exception e){
            // Handle any I/O problems
        }
        String matrix[][]  = new String[rowList.size()][];
        for (int i = 0; i < rowList.size(); i++) {
            String[] row = rowList.get(i);
            matrix[i] = row;


        }


    }
    public String[] parseFile() {
        Calendar cal = Calendar.getInstance(Locale.UK);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        String Data[][] = new String[140][27];
        String tempData[];

        StringBuilder sb = new StringBuilder();
        int i = 0;
        int numb = 0;
        try {
            //Объект для чтения файла в буфер
            BufferedReader in = new BufferedReader(new FileReader("/mnt/hdd/jenkins/workspace/AUTO.RIA.com.Allure/AUTO.RIA.com.Allure/src/test/java/ria/com/resources/catalogBase.csv"));
//            BufferedReader in = new BufferedReader(new FileReader("/var/www/auto_test/AUTO.RIA.comSEO/src/test/java/ria/com/resources/catalogBase.csv"));
            try {
                //В цикле построчно считываем файл
                String s;
                while ((s = in.readLine()) != null) {
//		        	  System.out.println(s);
                    //Парсим данные в масив
                    tempData = s.split(",");
                    if (i > 0) {
                        numb = i - 1;
                        Data[numb][0] = tempData[0];    //№
                        Data[numb][1] = tempData[1];    //Марка
                        Data[numb][2] = tempData[2];    //Модель
                        Data[numb][3] = tempData[3];    //Область
                        Data[numb][4] = tempData[4];    // Місто
                        Data[numb][5] = tempData[5];    // Країна
                        Data[numb][6] = tempData[6];    // Рік
                        Data[numb][7] = tempData[7];    // Тип транспорта
                        Data[numb][8] = tempData[8];    // Тип кузова
                        Data[numb][9] = tempData[9];    // Колір
                        Data[numb][10] = tempData[10];  // Тип топлива
                        Data[numb][11] = tempData[11];  // Об’єм
                        Data[numb][12] = tempData[12];  // Тип КПП
                        Data[numb][13] = tempData[13];  // Ціна
                    }

                    i++;
                }


            } finally {
                //Также не забываем закрыть файл
                in.close();
            }

        } catch (IOException e) {
            //throw new RuntimeException(e);
        }
        //Формируем уникальный индекс
        int correctKoef = 0;
        if (hour >= 7 && hour < 12) {
            correctKoef = 2;
        }
        if (hour >= 12 && hour < 17) {
            correctKoef = 1;
        }
        if ((hour >= 17 && hour < 24) || (hour >= 0 && hour < 7)) {
            correctKoef = 0;
        }

        int index = ((day * 3) - correctKoef) % 30;

//		    System.out.println("index="+index);

        String returnArr[] = new String[13];
        returnArr[0] = Data[index][1];
        returnArr[1] = Data[index][2];
        returnArr[2] = Data[index][3];
        returnArr[3] = Data[index][4];
        returnArr[4] = Data[index][5];
        returnArr[5] = Data[index][6];
        returnArr[6] = Data[index][7];
        returnArr[7] = Data[index][8];
        returnArr[8] = Data[index][9];
        returnArr[9] = Data[index][10];
        returnArr[10] = Data[index][11];
        returnArr[11] = Data[index][12];
        returnArr[12] = Data[index][13];


        return returnArr;
    }
}
