package com.auto.ui.tests;

import com.example.services.UserApiService;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.socks.ui.tests.BaseUiTest.read_json_from_url;
import static com.socks.ui.tests.BaseUiTest.write_to_file;

public class FindAutoWithoutPhoto {

    private final UserApiService userApiService = new UserApiService();

    @DataProvider(name = "photo_id", parallel = true)
    public static Iterator<Object[]> urls() throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/id_ads_photo_404.csv"));
        String line;
        List<Object[]> lines = new ArrayList<>();

        while ((line = in.readLine()) != null) {
            lines.add(new String[]{line});
        }

        return lines.iterator();

    }

    @Test(dataProvider = "photo_id")
    public void testSecondVersion(String id) throws Exception{

       // System.out.println(id);

        int autoId;
        int photoId;
        String small;

        JSONObject json = read_json_from_url("https://auto.ria.com/demo/bu/finalPage/photos/" + id + "");
        //JSONObject json = read_json_from_url("https://auto.ria.com/demo/bu/finalPage/photos/24083892");
        int count_photo = json.getJSONArray("photo").length();
        if(count_photo==0){
            System.out.println(id);
        }



    }

 /*   24307404
            24307384
    */

}
