package com.auto.ui.tests;

import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.socks.ui.tests.BaseUiTest.read_from_url_to_array;
import static com.socks.ui.tests.BaseUiTest.write_to_file;

public class SaveUserIdByAutoId {

    public static Connection connection;
    private static final String url = "jdbc:mysql://ovirt.ria.com:3321/";
    private static final String dbName = "auto3";
    private static final String mysqldriver = "com.mysql.cj.jdbc.Driver";
    private static final String user = "autotest";
    private static final String pass = "3g4F8I2ZhFCOOWcu";

    @Test
    public void rgr() throws Exception {


        System.out.println("----------");
        Arrays.stream(read_from_file(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/photo_id_result_without_dupes.txt")).forEach(k -> {
            //System.out.println(k);


            try {
                Class.forName(mysqldriver).newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            try {
                connection = DriverManager.getConnection(url+dbName, user, pass);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Statement stmt = null;
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = null;
            try {
                rs = stmt.executeQuery("select user_id from auto3.auto where auto_id = "+k+";");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    if (!rs.next()) break;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                String user_id = null;
                try {
                    user_id = rs.getString("user_id");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println(user_id);

            }
            //rs.close();
            //stmt.close();



        });

        System.out.println("CONNECTING TO DATABASE");

    }

    @Test(priority = 2)
    public void testDeleteDupesUserID() throws Exception {
        List<String> withoutDupes = Arrays.stream(read_from_url_to_array(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/id_user_photo.txt"))
                .distinct()
                .collect(Collectors.toList());

        withoutDupes.stream().forEach(k -> {
            System.out.println(k);
            try {
                write_to_file("/src/test/java/com/auto/ui/tests/user_id_result_without_dupes.txt", k);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    @Test(priority = 3)
    public void findActivAds() throws Exception {
        System.out.println("----------");
        Arrays.stream(read_from_file(System.getProperty("user.dir") + "/src/test/java/com/auto/ui/tests/photo_id_result_without_dupes.txt")).forEach(k -> {
            //System.out.println(k);


            try {
                Class.forName(mysqldriver).newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            try {
                connection = DriverManager.getConnection(url+dbName, user, pass);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            Statement stmt = null;
            try {
                stmt = connection.createStatement();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            ResultSet rs = null;
            try {
                rs = stmt.executeQuery("select user_id from auto3.auto where auto_id = "+k+" and status_id = 0; ");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            while (true) {
                try {
                    if (!rs.next()) break;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                String user_id = null;
                try {
                    user_id = rs.getString("user_id");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println(user_id);

            }
            //rs.close();
            //stmt.close();



        });

        System.out.println("CONNECTING TO DATABASE");
    }


    public static String[] read_from_file(String file) throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(
                file));
        String line;

        List<String> lines = new ArrayList<String>();
        while ((line = in.readLine()) != null) {
            lines.add(line);
        }
        String[] linesAsArray = lines.toArray(new String[lines.size()]);

        return linesAsArray;
    }

}
