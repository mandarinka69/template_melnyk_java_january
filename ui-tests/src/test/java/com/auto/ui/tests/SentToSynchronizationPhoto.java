package com.auto.ui.tests;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SentToSynchronizationPhoto {

    @BeforeClass
    public void setUp() {

        Configuration.browser = "chrome";
        Configuration.browserSize = "1366x768";
    }


    @DataProvider(name = "photo_id", parallel = true)
    public static Iterator<Object[]> urls() throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/src/test/java/com/socks/ui/tests/new_ads_id.csv"));
        String line;
        List<Object[]> lines = new ArrayList<>();

        while ((line = in.readLine()) != null) {
            lines.add(new String[]{line});
        }

        return lines.iterator();

    }

    @Test
    public void testAddToSync(){

    }
}
