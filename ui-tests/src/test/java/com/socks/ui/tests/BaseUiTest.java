package com.socks.ui.tests;

import com.github.javafaker.Faker;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BaseUiTest {
    protected final Faker faker = new Faker();


    public static String[] read_from_url_to_array(String file) throws Exception {
        BufferedReader in = new BufferedReader(new FileReader(
                file));
        String line;

        List<String> lines = new ArrayList<String>();
        while ((line = in.readLine()) != null) {
            lines.add(line);
        }
        String[] linesAsArray = lines.toArray(new String[lines.size()]);

        return linesAsArray;
    }


    public static void write_to_file(String dir, String text) throws Exception {
        try (FileWriter writer = new FileWriter(System.getProperty("user.dir") + dir, true)) {
// запись всей строки
            writer.write(text + "\n");

            writer.flush();
        } catch (IOException ex) {

            System.out.println(ex.getMessage());
        }
    }

    public static JSONObject read_json_from_url(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } catch (Exception e) {
            JSONObject json = new JSONObject();
            return json;
        } finally {
            is.close();
        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

}
