package com.socks.ui.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.example.ProjectConfig;
import com.example.model.UserPayload;
import com.example.services.UserApiService;
import com.socks.ui.LoggedUserPage;
import com.socks.ui.MainPage;
import io.restassured.RestAssured;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

import static com.codeborne.selenide.Selenide.*;

public class TestLogin extends BaseUiTest {

    private UserApiService userApiService = new UserApiService();

    @BeforeClass
    public void setUp() {
        Map myVars = new HashMap();
        myVars.put("env", System.getProperty("env", "prod"));

        RestAssured.baseURI = ConfigFactory.create(ProjectConfig.class, myVars).apiPath();

        Configuration.browser = "chrome";
        Configuration.browserSize = "1366x768";
    }

    @Test()
    public void userCanLoginWithValidCredentials() {
        //given
        UserPayload userPayload = new UserPayload()

                .setUsername(faker.name().username())
                .setPassword("12345")
                .setEmail("test@gmail.com");

        userApiService.registerUser(userPayload);

        //when
        MainPage mainPage = open("http://35.246.254.107/", MainPage.class);
        LoggedUserPage loggedUserPage =  mainPage.loginAs(userPayload.getUsername(), userPayload.getPassword());


        //then
        loggedUserPage.logoutBtn .shouldHave(Condition.text("Logout"));


    }

}
